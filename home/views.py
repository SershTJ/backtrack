from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate,logout

from django.urls import reverse_lazy

import random
# Create your views here.

from .models import Boner, Client, CarPart, Sale, User
from .forms import BonerForm, ClientForm, CustomUserCreationForm, CustomLoginForm, CustomUserChangeForm, CarPartsForm


def empleadosindex(request):
    user = request.user
    context ={
        "mensaje":"Un Mensaje",
        "user":user
    }
    return render(request,"Home/index.html",context)

# def register(response):
#     if response.method == "POST":
#         form = CustomUserCreationForm(response.POST)
#         if form.is_valid():
#             form.save()        
#             return redirect("/")
#     else:
#         form = CustomUserCreationForm()
#     return render(response,"Account/register.html",{"form":form})

def userLogout(request):
    logout(request) 
    return redirect("/catalogue/")

# def userLogIn(request):
#     if request.method == "POST":
#         form = CustomLoginForm(data=request.POST)
#         if form.is_valid():
#             user = form.get_user()
#             login(request,user)
#             return redirect("/")
#     else:
#         form = CustomLoginForm()
#     return render(request,"Account/login.html",{"form":form})   
    

# BONERS 
def boner(request):
    context ={
        "boners_list": Boner.objects.all()
    }
    return render(request,"Boner/index.html",context)

def bonerList(request,id):
    context ={
        "boners_list": Boner.objects.all()
    }
    return render(request,"Boner/index.html",context)

def bonerDetails(request,id):
    queryset = Boner.objects.get(Id=id)
    context={
        "object":queryset
    }
    return render(request,"Boner/detail.html",context)

def bonerCreate(request):
    form = BonerForm(request.POST or None)
    if request.method == "POST":
        if request.user.is_authenticated:
            if form.is_valid():
                form.save()
                return redirect("boner_view")
    else:
        error = "User Must Be Logged"
    context={
        "form":form,
        "message": error
    }
    return render(request,"Boner/create.html",context)


def bonerEdit(request,id):
    error = ""
    boner = Boner.objects.get(Id=id)
    if request.method == "GET":        
        form = BonerForm(instance=boner)
    else:
        form = BonerForm(request.POST,instance=boner)
        if form.is_valid():
            form.save()
            return redirect("boner_view")
        else:
            error = "Can't save"
    context={
        "form":form,
        "message": error
    }
    return render(request,"Boner/edit.html",context)

def bonerDelete(request,id):
    error = ""
    boner = Boner.objects.get(Id=id)
    if request.method == "POST":        
        boner.delete()

        return redirect("boner_view")
    context={
        "object":boner,
        "error":error
    }

    return redirect("boner_view")

# Clients

def client(request):
    context ={
        "clients_list": User.objects.filter(is_staff=0).all()
    }
    return render(request,"Client/index.html",context)

def clientList(request,id):
    context ={
        "clients_list": User.objects.filter(is_staff=0).all()
    }
    return render(request,"Client/index.html",context)

def clientDetails(request,id):
    queryset = User.objects.get(id=id)
    context={
        "object":queryset
    }
    return render(request,"Client/detail.html",context)

def clientCreate(request):
    form = CustomUserCreationForm(request.POST or None)
    if request.method == "POST":
        if request.user.is_authenticated:
            if form.is_valid():
                form.save()
                return redirect("client_view")
    else:
        error = "User Must Be Logged"
    context={
        "form":form,
        "message": error
    }
    return render(request,"Client/create.html",context)

def clientEdit(request,id):
    error = ""
    client = User.objects.get(id=id)
    if request.method == "GET":        
        form = CustomUserChangeForm(instance=client)
    else:
        form = CustomUserChangeForm(request.POST,instance=client)
        if form.is_valid():
            form.save()
            return redirect("client_view")
        else:
            error = form.errors
    context={
        "form":form,
        "message": error
    }
    return render(request,"Client/edit.html",context)

def clientDelete(request,id):
    error = ""
    client = User.objects.get(id=id)
    if request.method == "POST":        
        client.delete()

        return redirect("client_view")
    context={
        "object":client,
        "error":error
    }

    return redirect("client_view")    

# Employees
def employee(request):
    context ={
        "employees_list": User.objects.filter(is_staff=1).all()
    }
    return render(request,"Employee/index.html",context)

def employeeList(request,id):
    context ={
        "employees_list": Employee.objects.all(is_staff=1)
    }
    return render(request,"Employee/index.html",context)

def employeeDetails(request,id):
    queryset = User.objects.get(id=id)
    context={
        "object":queryset
    }
    return render(request,"Employee/detail.html",context)

def employeeCreate(request):
    form = CustomUserCreationForm(request.POST or None)
    if request.method == "POST":
        if request.user.is_authenticated:
            if form.is_valid():

                form.save()
                return redirect("employee_view")
    else:
        error = "User Must Be Logged"
    context={
        "form":form,
        "message": error
    }
    return render(request,"Employee/create.html",context)

def employeeEdit(request,id):
    error = ""
    employee = User.objects.get(id=id)
    if request.method == "GET":        
        form = CustomUserChangeForm(instance=employee)
    else:
        form = CustomUserChangeForm(request.POST,instance=employee)
        if form.is_valid():
            form.save()
            return redirect("employee_view")
        else:
            error = "Can't save"
    context={
        "form":form,
        "message": error
    }
    return render(request,"Employee/edit.html",context)

def employeeDelete(request,id):
    error = ""
    employee = Employee.objects.get(Id=id)
    if request.method == "POST":        
        employee.delete()

        return redirect("employee_view")
    context={
        "object":employee,
        "error":error
    }

    return redirect("employee_view") 


# CarParts
def carParts(request):
    context ={
        "carParts_list": CarPart.objects.all()
    }
    return render(request,"CarParts/index.html",context)

def carPartsList(request,id):
    context ={
        "carParts_list": CarPart.objects.all()
    }
    return render(request,"CarParts/index.html",context)

def carPartsDetails(request,id):
    queryset = CarPart.objects.get(Id=id)
    context={
        "object":queryset
    }
    return render(request,"CarParts/detail.html",context)

def carPartsCreate(request):
    error = ""
    form = CarPartsForm(request.POST or None)
    if request.method == "POST":
        if request.user.is_authenticated:
            if form.is_valid():
                form.save()
                return redirect("carParts_view")
    else:
        error = "User Must Be Logged"
    context={
        "form":form,
        "message": error
    }
    return render(request,"CarParts/create.html",context)

def carPartsEdit(request,id):
    error = ""
    carParts = CarPart.objects.get(Id=id)
    if request.method == "GET":        
        form = CarPartsForm(instance=carParts)
    else:
        form = CarPartsForm(request.POST,instance=carParts)
        if form.is_valid():
            form.save()
            return redirect("carParts_view")
        else:
            error = "Can't save"
    context={
        "form":form,
        "message": error
    }
    return render(request,"CarParts/edit.html",context)

def carPartsDelete(request,id):    
    error = ""
    carParts = CarPart.objects.get(Id=id)
    if request.method == "POST":        
        carParts.delete()

        return redirect("carParts_view")
    context={
        "object":carParts,
        "error":error
    }

    return redirect("carParts_view") 


def sales(request):
    context ={
        "Sales_list": Sale.objects.all(),
        "client_list": CarPart.objects.all()
    }
    return render(request,"Sale/index.html",context)
    
def salesAdd(request,id):
    user = request.user
    querysetClient = CarPart.objects.get(Id=id)
    temp = Sale(detail=user.first_name,IdUser=user,IdCar=querysetClient,Quantity=1)
    temp.save()
    context ={
         "carParts_list": CarPart.objects.all()
    }
    return render(request,"CarParts/index.html",context)

def increaseSale(request,id):
    oldquantity = Sale.objects.get(Id=id)
    Sale.objects.filter(Id=id).update(Quantity=oldquantity.Quantity + 1)
    context ={
        "Sales_list": Sale.objects.all(),
        "client_list": CarPart.objects.all()
    }
    return redirect('sales_view')

def decreaseSale(request,id):
    oldquantity = Sale.objects.get(Id=id)
    Sale.objects.filter(Id=id).update(Quantity=oldquantity.Quantity - 1)
    context ={
        "Sales_list": Sale.objects.all(),
        "client_list": CarPart.objects.all()
    }
    return redirect('sales_view')

def deleteSale(request,id):
    Sale.objects.filter(Id=id).delete()

    context ={
    }
    return render(request,"Sale/index.html",context)

