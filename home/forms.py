from django import forms
from django.conf import settings
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.contrib.auth import get_user_model

from .models import CarPart,Boner,Client,Car,Sale,User
    
class BonerForm(forms.ModelForm):
    Name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    Phone = forms.CharField(widget=forms.NumberInput(attrs={"class":"form-control"})) 
    Address = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    PostalCode = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    Mail = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    Supervisor = forms.ModelChoiceField(queryset=User.objects.filter(is_staff=1).all(),widget=forms.Select(attrs={"class":"form-control"}))
    class Meta:
        model = Boner
        fields = [
            "Name",
            "Phone",
            "Address",
            "PostalCode",
            "Mail",
            "Supervisor"
        ]

class ClientForm(forms.ModelForm):
    Name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    Phone = forms.CharField(widget=forms.NumberInput(attrs={"class":"form-control"})) 
    Address = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    Mail = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    class Meta:
        model = Client
        fields = [
            "Name",
            "Phone",
            "Address",
            "Mail",
        ]

class CarPartsForm(forms.ModelForm):
    Name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    Model = forms.CharField(widget=forms.NumberInput(attrs={"class":"form-control"})) 
    Year = forms.DateField(widget=forms.DateTimeInput(attrs={"class":"form-control"})) 
    Color = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    Stock = forms.CharField(widget=forms.NumberInput(attrs={"class":"form-control"})) 
    Boner =forms.ModelChoiceField(queryset=Boner.objects.all(),widget=forms.Select(attrs={"class":"form-control"}))
    class Meta:
        model = CarPart
        fields = [
            "Name",
            "Model",
            "Year",
            "Color",
            "Stock",
            "Boner",
        ]

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = get_user_model()
        fields = UserCreationForm.Meta.fields + ('first_name','last_name','phone','address','email','is_staff',)
        
        

        # fields = UserCreationForm.Meta.fields + ('first_name','last_name','phone','address','email')

class CustomUserChangeForm(UserChangeForm):
    username = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    first_name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    last_name = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    phone = forms.CharField(widget=forms.NumberInput(attrs={"class":"form-control"})) 
    address = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    email = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"})) 
    # is_employee = forms.BooleanField(widget=forms.CheckboxInput(attrs={"class":"form-control"}), required=False)
    is_staff = forms.BooleanField(widget=forms.CheckboxInput(attrs={"class":"form-control"}), required=False)

    class Meta:
        model = get_user_model()
        fields = UserCreationForm.Meta.fields + ('first_name','last_name','phone','address','email','is_staff',)

        # fields = UserCreationForm.Meta.fields + ('first_name','last_name','phone','address','email')

class CustomLoginForm(AuthenticationForm):

    class Meta(AuthenticationForm):
        model = get_user_model()
        fields = ['username','password']