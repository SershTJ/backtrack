from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class CarPart(models.Model):
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=100)
    Model = models.CharField(max_length=100)
    Year = models.DateField()
    Color = models.CharField(max_length=100)
    Stock = models.IntegerField()
    Boner = models.ForeignKey('Boner',on_delete=models.SET_NULL,null=True)

    def __str__(self):
        return self.Name


class Boner(models.Model):
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=150)
    Phone = models.CharField(max_length=12)
    Address = models.CharField(max_length=300)
    PostalCode = models.CharField(max_length=150)
    Mail = models.CharField(max_length=150)
    Supervisor = models.ForeignKey('User',on_delete=models.SET_NULL,null=True)

    def __str__(self):
        return self.Name

class User(AbstractUser):
    phone = models.CharField(max_length=12)
    address = models.CharField(max_length=300)
    # is_employee = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    

    def say_hello(self):
        return "Hello, my name is {}".format(self.first_name)    

class Client(models.Model):
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=150)
    LastName = models.CharField(max_length=150)
    Phone = models.CharField(max_length=12)
    Address = models.CharField(max_length=300)
    Mail = models.CharField(max_length=150)

    def __str__(self):
        return self.Name

class Car(models.Model):
    Id = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=150)
    Model = models.CharField(max_length=150)
    Manufacturer = models.CharField(max_length=100)

    def __str__(self):
        return self.Name

class Sale(models.Model):
    Id = models.AutoField(primary_key=True)
    detail = models.CharField(max_length=150)
    IdClient = models.ForeignKey('Client',on_delete=models.SET_NULL,null=True)
    IdUser = models.ForeignKey('User',on_delete=models.SET_NULL,null=True)
    IdCar = models.ForeignKey('CarPart',on_delete=models.SET_NULL,null=True)
    Quantity = models.IntegerField()

    def __str__(self):
        return self.detail 
    
    def save(self, *args, **kwargs):   
        self.detail,self.IdClient,self.IdUser,self.IdCar,self.Quantity
        super(Sale, self).save(*args, **kwargs)