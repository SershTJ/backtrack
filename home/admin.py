from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

# Register your models here.


from .forms import CustomUserCreationForm, CustomUserChangeForm


from .models import CarPart
from .models import Boner
from .models import User
from .models import Client
from .models import Car
from .models import Sale

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ['email', 'username','phone','address','is_staff']

admin.site.register(User, CustomUserAdmin)
admin.site.register(CarPart)
admin.site.register(Boner)
admin.site.register(Client)
admin.site.register(Car)
admin.site.register(Sale)

