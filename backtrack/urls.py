"""backtrack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include


from django.apps import apps

from django.conf.urls import include, url  # < Django-2.0
# from django.urls import include, path  # > Django-2.0
from django.conf.urls.static import static

from django.conf import settings

from home import views



# from backtrack.app import application
# 
urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', views.index, name='index_view'),
    path('CEmpleados/', views.empleadosindex, name='empleadosindex_view'),

    path('Boner/', views.boner, name='boner_view'),
    path('Boner/Details/<int:id>', views.bonerDetails, name='bonerDetail_view'),
    path('Boner/Create/', views.bonerCreate, name='bonerCreate_view'),
    path('Boner/Edit/<int:id>', views.bonerEdit, name='bonerEdit_view'),
    path('Boner/Delete/<int:id>', views.bonerDelete, name='bonerDelete_view'),

    path('Client/', views.client, name='client_view'),
    path('Client/Details/<int:id>', views.clientDetails, name='clientDetail_view'),
    path('Client/Create/', views.clientCreate, name='clientCreate_view'),
    path('Client/Edit/<int:id>', views.clientEdit, name='clientEdit_view'),
    path('Client/Delete/<int:id>', views.clientDelete, name='clientDelete_view'),

    path('Employee/', views.employee, name='employee_view'),
    path('Employee/Details/<int:id>', views.employeeDetails, name='employeeDetail_view'),
    path('Employee/Create/', views.employeeCreate, name='employeeCreate_view'),
    path('Employee/Edit/<int:id>', views.employeeEdit, name='employeeEdit_view'),
    path('Employee/Delete/<int:id>', views.employeeDelete, name='employeeDelete_view'),

    path('CarParts/', views.carParts, name='carParts_view'),
    path('CarParts/Details/<int:id>', views.carPartsDetails, name='carPartsDetail_view'),
    path('CarParts/Create/', views.carPartsCreate, name='carPartsCreate_view'),
    path('CarParts/Edit/<int:id>', views.carPartsEdit, name='carPartsEdit_view'),
    path('CarParts/Delete/<int:id>', views.carPartsDelete, name='carPartsDelete_view'),

    path('Sales/', views.sales, name='sales_view'),
    path('Sales/Add/<int:id>', views.salesAdd, name='addCart_view'),
    path('Sales/increase/<int:id>', views.increaseSale, name='increase_view'),
    path('Sales/decrease/<int:id>', views.decreaseSale, name='decrease_view'),
    path('Sales/delete/<int:id>', views.deleteSale, name='delete_view'),

    # path('register/', views.register, name='register_view'),
    path('logout/', views.userLogout, name='logout_view'),
    # path('login/', views.userLogIn, name='login_view'),







    url(r'^i18n/', include('django.conf.urls.i18n')),
    # path('i18n/', include('django.conf.urls.i18n')),  # > Django-2.0

    # The Django admin is not officially supported; expect breakage.
    # Nonetheless, it's often useful for debugging.

    url(r'^admin/', admin.site.urls),
    # path('admin/', admin.site.urls),  # > Django-2.0

    url(r'^', include(apps.get_app_config('oscar').urls[0])),
    # path('', include(apps.get_app_config('oscar').urls[0])),  # > Django-2.0


    # url(r'^', include(apps.get_app_config('home').urls[0])),


     # url(r'', include(application.urls)),



]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
